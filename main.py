import requests
import random

genre = input("What genre are you interested in?")


def get_movie(genre):
    """This is a function that gives you a random movie based on the genre you input using a API of movies."""

    movies = requests.get(f"https://www.omdbapi.com/?apikey=8aa731ae&s={genre}&type=movie&page=100").json()['Search']
    random_movie = movies[random.randint(0, len(movies))]
    print(random_movie['Title'])
    return random_movie


if __name__ == "__main__":
    try:
        get_movie(genre)
    except ValueError:
        print("Genre does not exist. Please try again!")
    except KeyError:
        print("Genre does not exist. Please try again!")
